<?php
	require __DIR__ . '/vendor/autoload.php';

	use Facebook\InstantArticles\AMP\AMPArticle;

	$client = new MongoDB\Client("mongodb://mongo:27017");

	try {
		$client->listDatabases();
	}
	catch( MongoDB\Driver\Exception\ConnectionTimeoutException $e ) {
		exit($e->getMessage());
	}

	$collection = $client->admin->instantarticles;

	foreach( $collection->find(['status' => 'valid']) as $document )
	{
		$amp = AMPArticle::create($document->content, [])->render();

		$client->admin->articles->updateOne(
			['refId' => $document->_id],
			['$set' => [
				'title' => $document->title,
				'link' => $document->link,
				'description' => $document->description,
				'author' => $document->author,
				'pubDate' => $document->pubDate,
				'content' => $amp,
				'type' => 'ia',
				'refId' => $document->_id
			]],
			['upsert' => true]
		);

		$collection->updateOne(
			['_id' => $document->_id],
			['$set' => ['status' => 'success']]
		);
	}
?>
