FROM nathanson/instant-articles-sdk:latest

RUN ["apk", "update"]
RUN apk add autoconf openssl-dev g++ make
RUN pecl install mongodb
RUN docker-php-ext-enable mongodb
RUN apk del --purge autoconf openssl-dev g++ make

RUN ["composer", "require", "mongodb/mongodb", "--ignore-platform-reqs"]

COPY /app/main.php /app

CMD ["php", "main.php"]
